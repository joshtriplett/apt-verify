The directory `/etc/apt/verify.d` is used by `apt-verify(8)`.

Normally this directory contains symbolic links to executable files,
to make it easy for sysadmins to enable (ln -s) and disable (rm) each
tool, but it is permitted to put a executable directly in this
directory.

Programs are executed ordered by a sorted name, and if one program
fails, the remaining programs will never be invoked.  Non-executable
files such as this README are ignored.

Apt parses GnuPG `gpgv(1)` outputs for signature verification status,
so you want to have a symbolic link `gpgv` in this directory pointing
to `/usr/bin/gpgv` or whatever `gpgv` you wish to use.

Apt ignores the exit code of these processes and only parse any GnuPG
`gpgv` outputs.  So if you place a program whose name sort after
`gpgv`, exiting with non-zero is not sufficient to cause apt to fail.

For the above reasons, we suggest placing any non-gpgv programs you
wish to be run under symbolic links prefixed with digits, like this:

```
$ ls -la /etc/apt/verify.d/
total 16
drwxr-xr-x  2 root root 4096 Feb  7 17:57 .
drwxr-xr-x 10 root root 4096 Feb  7 17:24 ..
-rwxr-xr-x  1 jas  jas  2065 Feb  7 17:57 10canary
-rw-r--r--  1 root root 1385 Feb  7 17:57 README
lrwxrwxrwx  1 root root   13 Feb  7 17:24 gpgv -> /usr/bin/gpgv
$
```

Happy Hacking!
